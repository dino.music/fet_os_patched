FROM registry.gitlab.com/amer.hasanovic/fet_os:latest

RUN apt-get update && apt-get upgrade -y && \
      DEBIAN_FRONTEND=noninteractive apt-get -y install  libsdl1.2-dev libtool-bin libglib2.0-dev libz-dev libpixman-1-dev &&\
      apt -y remove qemu-user qemu-system-x86
RUN git clone https://github.com/mit-pdos/6.828-qemu.git /opt1/qemu
RUN cd /opt1/qemu && ./configure --disable-kvm --disable-werror --target-list="i386-softmmu x86_64-softmmu" && \
      make && make install 
